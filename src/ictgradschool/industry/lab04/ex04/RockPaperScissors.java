package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

import javax.rmi.CORBA.Tie;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;

    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.

        // Get user name
        System.out.print("Hi!What is your name? ");
        String userName = Keyboard.readInput();

        outLoop:
        while(true) {
            //Get user choice
            System.out.println("\n1. Rock\n2. Scissors\n3. Paper\n4. Quit");
            System.out.print("      Enter choice:");
            int userChoice = Integer.parseInt(Keyboard.readInput());
            System.out.print("\n");

            //play or not?
            if (userChoice ==4) {
                System.out.print("Goodbye "+userName+". Thanks for playing :)");
                break;
            }

            //Set computer name and choice
            String computerName = "Computer";
            int computerChoice = (int) (Math.random() * 3 + 1);

            //Display players choice
            displayPlayerChoice(userName, userChoice);
            displayPlayerChoice(computerName, computerChoice);

            if (userChoice>1&&userChoice<4) {

                //Get Winner
                String Winner;
                if (userWins(userChoice, computerChoice)) {
                    Winner = userName;
                } else {
                    Winner = "The " + computerName;
                }

                //Get Result
                String result = getResultString(userChoice, computerChoice);

                // Display Result
                if (result.equals(" you chose the same as the computer")) {
                    System.out.println("No one wins.");
                } else {
                    System.out.println(Winner + " Wins because " + result + ".");
                }
            }
        }

    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        if (choice == 1){
            System.out.println(name+" chose rock");
        }else if (choice == 2){
            System.out.println(name+" chose scissors");
        }else if (choice ==3) {
            System.out.println(name+" chose paper");
        }else {
            System.out.println(name+" enter a invalid choice");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.

        switch (playerChoice){
            case 1:
                if(computerChoice == 2){
                    return true;
                } else{
                    return false;
                }
            case 2:
                if(computerChoice == 3){
                    return true;
                } else{
                    return false;
                }
            case 3:
                if(computerChoice == 1){
                    return true;
                } else{
                    return false;
                }
        }
        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";



        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == computerChoice){
            return TIE;
        }else {
            switch (playerChoice) {
                case 1:
                    if (computerChoice == 2) {
                        return ROCK_WINS;
                    } else {
                        return PAPER_WINS;
                    }
                case 2:
                    if (computerChoice == 3) {
                        return SCISSORS_WINS;
                    } else {
                        return ROCK_WINS;
                    }
                case 3:
                    if (computerChoice == 1) {
                        return PAPER_WINS;
                    } else {
                        return SCISSORS_WINS;
                    }
            }
        }
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
