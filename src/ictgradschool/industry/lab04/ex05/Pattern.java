package ictgradschool.industry.lab04.ex05;

/**
 * Created by qpen546 on 14/03/2017.
 */
public class Pattern {
    private int number;
    private char symbol;

    public Pattern(int number, char symbol) {
        this.number = number;
        this.symbol = symbol;
    }

    public String toString (){
        String info ="";
        for (int i=0;i<number;i++){
            info += symbol;
        }
        return info;
    }

    public int getNumberOfCharacters (){
        return number;
    }

    public void setNumberOfCharacters (int number){
        this.number = number;
    }

}
