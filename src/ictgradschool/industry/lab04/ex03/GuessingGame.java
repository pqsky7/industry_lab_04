package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

        boolean result = false;
        int goal = getRandomNumber();
        while(!result) {
            int guss = getUserInput();
            result = printResult(goal,guss);
        }
        System.out.println("Goodbye");
    }

    private int getRandomNumber () {
        int randomNumber = (int)(Math.random()*100+1);
        return randomNumber;
    }

    private int getUserInput (){
        System.out.print("Enter your guess: ");
        return Integer.parseInt(Keyboard.readInput());
    }

    private boolean printResult (int goal,int guss){
        boolean result = false;
        if (guss == goal){
            result = true;
            System.out.println("Perfect!");
        }else if (guss < goal) {
            System.out.println("Too low, try again");
        }else{
            System.out.println("Too high, try again");
        }
        return result;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
